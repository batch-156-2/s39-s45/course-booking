import {useContext} from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

function AppNavBar() {
	const {user} = useContext(UserContext);
	return(
		<Navbar bg="primary" expand="lg">
		<Container>
			<Navbar.Brand>B156 Booking App</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse>
				<Nav className="ml-auto">
					<Link className="nav-link" to="/">
						Home
					</Link>
					{
						user.id !== null ? <Link className="nav-link" to="/logout">Logout</Link>
						: <><Link className="nav-link" to="/login">Login</Link>
							<Link className="nav-link" to="/register">Register</Link>
							</>
					
					}
					<Link className="nav-link" to="/courses">
						Courses
					</Link>
					<Link className="nav-link" to="/courses/create">
						Create
					</Link>
					<Link className="nav-link" to="/courses/update">
						Update
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Container>
		</Navbar>
	);
};

export default AppNavBar;