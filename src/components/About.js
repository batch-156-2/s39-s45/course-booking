import {Row, Col, Card, Container} from 'react-bootstrap'

export default function About() {
	return(
		<Row className="p-5 rowBg">
			<Col xs={10} md={8} lg={8}>
				<h4>About the Author:</h4>
				<h2 className="mb-3">Miguel Oriel Carvajal</h2>
				<h5>Full Stack Developer</h5>
				<p className="my-3">Registered Nurse, who specialized in the Hemodialysis Unit for 1 year. I also had 5 months of experience working in the Medical Ward. I am currently taking coding and programming training under the supervision of Zuitt, Co. I have always been interested in technology and everything related to it, and with times changing now due to the pandemic, I believe it would also be a good time for me to enter a new specialization. With the skills and habits I picked up with my experience as a Nurse, I believe it would help me greatly to perform really well even if it's under a new branch of science.</p>
				<h5>Contact:</h5>
				<ul>
					<li>Email: mdnoxdeus@gmail.com</li>
					<li>Mobile Number: 09175820805</li>
					<li>Address: Bacnotan, La Union</li>
				</ul>
			</Col>
		</Row>
	);
};