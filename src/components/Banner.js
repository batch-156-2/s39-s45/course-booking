import {Row, Col} from 'react-bootstrap';

export default function Banner({bannerData}) {
	return(
		<div className="p-5 text-center">
			<Col>
				<h1>{bannerData.title}</h1>
				<p className="my-3">{bannerData.content}</p>
				{/*<a className="btn btn-primary p-2" href="/">Insert Action Here</a>*/}
			</Col>
		</div>
	);
};
