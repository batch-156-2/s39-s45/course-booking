import {Row, Col, Card, Container} from 'react-bootstrap'

export default function Highlights() {
	return(
		<Container>
			<Row className="text-center p-5">
			<Col xs={12} md={4}>
				<Card className="p-4 cardHighlight">
					<Card.Body>
						<Card.Title>Learn from Home</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="p-4 cardHighlight">
					<Card.Body>
						<Card.Title>Study now, Pay later.</Card.Title>
						<Card.Text>
							Lorem ipsum dolor, sit amet consectetur adipisicing.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="p-4 cardHighlight">
					<Card.Body>
						<Card.Title>Friendly community.</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, itaque.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			</Row>
		</Container>	
	);
};