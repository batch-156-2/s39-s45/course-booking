import Hero from './../components/Banner';
import {Form, Row, Col, Card, Button, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

const data = {
	title: 'Course creation page.',
	content: 'Create a new course here.'
}

export default function createCourse() {
	const createCourse = (event) => {
		event.preventDefault()
		return(
			Swal.fire({
				icon: 'success',
				title: 'Course successfully created!',
				text: 'Course added to the catalog.'
			})
		);
	};
	return(
		<div>	
			<Hero bannerData={data}/>
			<Row >
				<Col>
					<Container>
						<h1 className="text-center" >Creation Form</h1>
						<Form onSubmit={e => createCourse(e)}>
							<Form.Group>
								<Form.Label>Course Name:</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter course name"
									required
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Course Description:</Form.Label>
								<Form.Control 
									type="text"
									placeholder="Enter description here" 
									required
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Price:</Form.Label>
								<Form.Control 
									type="number"
									placeholder="Insert price here"
									required
								/>
							</Form.Group>

							<Button className="btn-block" type="submit">Create</Button>
						</Form>
					</Container>
				</Col>
			</Row>
		</div>
	);
};