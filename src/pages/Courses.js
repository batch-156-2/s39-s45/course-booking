import {useState, useEffect} from 'react'
import Hero from './../components/Banner';
import CourseCard from './../components/CourseCard';
import {Container} from 'react-bootstrap'

const bannerDetails = {
	title: 'Course Catalog.',
	content: 'Browse through our catalog of courses.'
}

export default function Courses() {
	const [coursesCollection, setCourseCollection] = useState([]);
	useEffect(() => {
		fetch('https://aqueous-badlands-89032.herokuapp.com/courses/active').then(res => res.json()).then(convertedData => {
			setCourseCollection(convertedData.map(course => {
				return(<CourseCard key={course._id} courseProp={course}/>)
			})) 
		});
	},[]);

	return(
		<div className="text-center">
			<Hero bannerData={bannerDetails}/>
			<Container>
				{coursesCollection}
			</Container>
		</div>
	)
}