import Hero from './../components/Banner';
import {useState, useEffect} from 'react'
import {Row, Col, Card, Button, Container} from 'react-bootstrap'
import {Link, useParams} from 'react-router-dom';
import Swal from 'sweetalert2';

const data = {
	title: 'Course',
	content: 'Course details found below.'
}

export default function CourseView() {
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	const {id} = useParams()
	useEffect(() => {
		fetch(`https://aqueous-badlands-89032.herokuapp.com/courses/${id}`)
		.then(res => res.json())
		.then(convertedData => {
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			});
		});
	},[id]);

	const enroll = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Enrolled successfully!',
				text: 'Thank you for enrolling to this course.'
			})
		);
	};
	return(
		<div>	
			<Hero bannerData={data}/>
			<Row>
				<Col>
					<Container>
						<Card className="text-center p-3">
						<Card.Body>
						{/*Course Name*/}
							<Card.Title>
								<h3>{courseInfo.name}</h3>
							</Card.Title>
						{/*Course Description*/}
							<Card.Subtitle>
								<h6 className="my-4">Description: </h6>
							</Card.Subtitle>
							<Card.Text>
								<h5>{courseInfo.description}</h5>
							</Card.Text>
						{/*Course Price*/}
							<Card.Subtitle>
								<h6 className="my-4">Price: </h6>
							</Card.Subtitle>
							<Card.Text>
								<h5>PHP {courseInfo.price}</h5>
							</Card.Text>
						</Card.Body>

						<Button className="btn-block" onClick={enroll}>
							Enroll
						</Button>

						<Link className="btn btn-secondary btn-block" to="/login">
							Login to Enroll
						</Link>
						<Link className="btn btn-warning btn-block" to="/courses/update">
							Update Course
						</Link>
						</Card>
					</Container>
				</Col>
			</Row>
		</div>
	);
};