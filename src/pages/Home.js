import Banner from	'./../components/Banner'
import Highlights from	'./../components/Highlights'

const details = {
	title: 'Welcome to the Home Page',
	content: 'Oppurtunities for everyone, everywhere.'
}

export default function Home() {
	return(
		<div>
			<Banner bannerData={details}/>
			<Highlights/>
		</div>
	);
};