import {useState, useEffect, useContext} from 'react'
import Banner from	'./../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom'
import swal from 'sweetalert2';
import UserContext from '../UserContext'

const details = {
	title: 'Registration page.',
	content: 'Create your account here.'
}

export default function Register() {

	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState(''); 

	const [isRegistered, setIsRegistered] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isDigits, setIsDigits] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);

	useEffect(() => {
		if (mobileNo.length === 11) {
			setIsDigits(true)
			if (password1 === password2 && password1 !== '' && password2 !== '') {
				setIsMatched(true);
				if (firstName !== '' && lastName !== '' && email !== '') {
					setIsAllowed(true);
					setIsRegistered(true);
				} else {
					setIsAllowed(false);
					setIsRegistered(false);
				}
			}
		} else if (password1 !== '' &&password1 === password2) {
			setIsMatched(true);
		} else {
			setIsDigits(false);
			setIsRegistered(false);
			setIsMatched(false);
			setIsAllowed(false);
		};
	},[firstName, lastName, email, password1, password2, mobileNo]);

	const registerUser = async(eventSubmit) => {
		eventSubmit.preventDefault()

		const isRegistered = await fetch('https://aqueous-badlands-89032.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})
		}).then(res => res.json()).then(jsondata => {
			if (jsondata.email) {
				return true;
			} else {
				return false;
			}
		})

		if (isRegistered) {
			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');
			await swal.fire({
				icon: 'success',
				title: 'Registration successful',
				text: 'Thank you for registering.'
			})
			window.location.href = "/login";
		} else {
			swal.fire({
				icon: 'error',
				title: 'Something went wrong.',
				text: 'Try again later.'
			});
		};
	};


	return(
		user.id ? <Navigate to="/" replace={true} />
		:
		<>
			<Banner className="text-center" bannerData={details}/>
			<Container>
				{
					isAllowed ? <h1 className="text-center text-success">You may now register.</h1>
					: <h1 className="text-center">Register Form</h1>
				}
				<h6 className="text-center mt-3 text-secondary">
					Fill up the form below!
				</h6>
				<Form onSubmit={e => registerUser(e)}>
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Enter your first name"
							required
							value={firstName}
							onChange={e => setFirstName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter your last name" 
							required
							value={lastName}
							onChange={e => setLastName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control 
							type="email"
							placeholder="Insert email here"
							required
							value={email}
							onChange={e => setEmail(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Mobile Number:</Form.Label>
						<Form.Control 
							type="number"
							placeholder="Insert your mobile number"
							required
							value={mobileNo}
							onChange={e => setMobileNo(e.target.value)}
						/>
						{
							isDigits ? <span className="text-success">Mobile Number is valid.</span>
							: <span className="text-muted">Mobile Number invalid.</span>
						}
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Enter password here"
							required
							value={password1}
							onChange={e => setPassword1(e.target.value)}
						/>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm Password:</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Confirm your password"
							required
							value={password2}
							onChange={e => setPassword2(e.target.value)}
						/>
						{
							isMatched ? <span className="text-success">Password match.</span>
							: <span className="text-danger">Password should match.</span>
						}
					</Form.Group>
						{
							isRegistered ? <Button className="btn-block" type="submit">Register</Button>
							: <Button className="btn-secondary btn-block" disabled>Register</Button>
						}
				</Form>
			</Container>
		</>
	);
};