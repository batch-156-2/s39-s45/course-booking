import {useState} from 'react';
import Banner from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Update Course',
	content: 'Update course details below.'
};

export default function updateCourse() {
	const [courseName, setCourseName] = useState('');
	const [courseDescription, setcourseDescription] = useState('');
	const [coursePrice, setCoursePrice] = useState('');
	const update = (event) => {
		event.preventDefault()
		return(
			Swal.fire({
				icon: 'success',
				title: 'Course updated successfully.',
				text: 'Check updates at the course catalog.'
			})
		);
	}

	return(
		<div>
			<Banner bannerData={data}/>
			<Container>
				<Form onSubmit={e => update(e)}>
					<Form.Group>
						<Form.Label>Name:</Form.Label>
						<Form.Control
							type="text"
							placeholder="Update course name here"
							required
							value={courseName}
							onChange={e => {setCourseName(e.target.value)}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control
							type="text"
							placeholder="Update course description here"
							required
							value={courseDescription}
							onChange={e => {setcourseDescription(e.target.value)}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control
							type="number"
							placeholder="Update course price here"
							required
							value={coursePrice}
							onChange={e => {setCoursePrice(e.target.value)}}
						/>
					</Form.Group>

					<Button className="btn-block" type="submit">Update</Button>

					<Form>
					  <Form.Check 
					    type="switch"
					    id="custom-switch"
					    label="Enable course"
					  />
					</Form>
				</Form>
			</Container>
		</div>
	);
};