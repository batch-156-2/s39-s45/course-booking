import {UserProvider} from './UserContext.js';
import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Create from './pages/AddCourse';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Update from './pages/UpdateCourse';
import Error from './pages/Error';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    })
  }
  useEffect(() => { 
    let token = localStorage.getItem('accessToken');
    fetch('https://aqueous-badlands-89032.herokuapp.com/users/userId', {
        headers: {
          Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(convertedData => {
      if (typeof  convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id, 
          isAdmin: convertedData.isAdmin
        });
      } else {
        setUser({
          id: null, 
          isAdmin: null
        });
      }
    });
  },[user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
         <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/logout' element={<Logout/>}/>
          <Route path='/courses/create' element={<Create/>}/> 
          <Route path='/courses' element={<Courses/>}/>
          <Route path='/courses/view/:id' element={<CourseView/>}/>
          <Route path='/courses/update' element={<Update/>}/>
          <Route path='*' element={<Error/>}/>
        </Routes>
      </Router> 
    </UserProvider>
  );
};

export default App;